// urlParser project main.go
package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"

	"golang.org/x/net/html"
)

type SinglePageCrawler struct {
}

//поиск аттрибута href
func (spc SinglePageCrawler) getHref(token html.Token) (ok bool, href string) {

	for _, attr := range token.Attr {
		if attr.Key == "href" {
			href = attr.Val
			ok = true
		}
	}

	return
}

//реализация интерфейса поиска токена a

func (spc SinglePageCrawler) SearchUrl(body io.ReadCloser) (urls []string) {

	tokenizer := html.NewTokenizer(body)

	for {
		nextToken := tokenizer.Next()

		switch {
		case nextToken == html.ErrorToken: //конец документа

			return
		case nextToken == html.StartTagToken: //просмотр очередного токена

			currentToken := tokenizer.Token()
			if isAnchor := currentToken.Data == "a"; !isAnchor {
				continue
			}

			ok, url := spc.getHref(currentToken)
			if !ok {
				continue
			}

			if isUrl := strings.Index(url, "http") == 0; isUrl {
				urls = append(urls, url)
			}
		}
	}
}

func main() {
	var initUrl string
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("Ошибка: не задан аргумент! Аргумент задается без - сразу после вызова приложения, например, X http://example.com")
		}
	}()

	initUrl = os.Args[1]

	response, err := http.Get(initUrl) //получаем страницу гет запросом

	if err != nil {
		fmt.Println("Ошибка парсинга сайта. Проверьте правильность адреса")
		return
	}

	b := response.Body //если нет ошибки, получаем тело страницы
	defer b.Close()

	fmt.Println("Поиск ссылок: " + initUrl)
	spc := SinglePageCrawler{}
	fdUrls := spc.SearchUrl(b)

	for num, url := range fdUrls {
		fmt.Println(strconv.Itoa(num) + " - " + url + " ")
	}

}
