package main

import (
	"bytes"
	"io"
	_ "os"
	_ "runtime"
	"testing"

	"golang.org/x/net/html"
)

type ClosingBuffer struct {
	*bytes.Buffer
}

func (cb *ClosingBuffer) Close() (err error) {
	return
}

func TestSearchUrl(t *testing.T) {
	var v []string
	cb := &ClosingBuffer{bytes.NewBufferString("Hi!")}
	var rc io.ReadCloser
	rc = cb
	rc.Close()
	spc := SinglePageCrawler{}

	v = spc.SearchUrl(rc)
	if len(v) > 0 {
		t.Error("Expected 0 elements, got ", v)
	}

	cb = &ClosingBuffer{bytes.NewBufferString(`<a href="http://example.com"> </a>`)}
	rc = cb
	rc.Close()

	v = spc.SearchUrl(rc)
	if v[0] != "http://example.com" {
		t.Error("Expected http://example.com, got ", v)
	}

	rights := [6]string{"http://example.com", "https://yandex.ru", "http://google.com", "https://bing.com", "http://vk.com", "http://gosuslugi.ru"}
	cb = &ClosingBuffer{bytes.NewBufferString(`<html><head></head><body>
	<a href="http://example.com"> </a>
	<div>
		<a href="https://yandex.ru">Яндекс</a>
		<div>
			<a href="http://google.com"></a>
		</div>
		<div>
			<a href="https://bing.com"></a>
			<div>
				<a href="http://vk.com"></a>
			</div>
		</div>
	</div>
	<a href="http://gosuslugi.ru"></a>
	<body></html>`)}
	rc = cb
	rc.Close()
	v = spc.SearchUrl(rc)
	for num, val := range v {
		if rights[num] != val {
			t.Error("Expected ", rights[num], " got ", val)
		}
	}
}

func TestgetHref(t *testing.T) {
	var attrs []html.Attribute
	var attr html.Attribute
	attr.Key = "href"
	attr.Val = "http://yandex.ru"

	attrs = append(attrs, attr)

	/*attr.Key = ""
	attr.Val = ""
	attrs = append(attrs, attr)*/

	token := html.Token{Attr: attrs}
	spc := SinglePageCrawler{}
	b, v := spc.getHref(token)
	if (v != "http://yandex.ru") && (!b) {
		t.Error("Expected v=http://yandex.ru and b is true got v=", v, " and b=", b)
	}

	attr.Key = "src"
	attr.Val = "http://yandex.ru"

	attrs = append(attrs, attr)

	token.Attr = attrs
	b, v = spc.getHref(token)
	if (v != "") && (b) {
		t.Error("Expected v is empty and b is false got v=", v, " and b=", b)
	}

}
